# Ceredi Simone es2 Progetto Programmazione di Reti a.a. 2019/2020

import signal
import sys
from socket import *
from threading import Thread


def request_controller(connection_socket):
    try:
        message = connection_socket.recv(1024)  # riceve il messaggio di richiesta dal client
        if len(message.split()) > 0:  # controlla che il messaggio non sia vuoto
            print(message, "::", message.split()[0], ":", message.split()[1])
            # stampa a video il messaggio di richiesta del client GET...
            filename = message.split()[1]
            print(filename, "||", filename[1:])
            f = open(filename[1:], "rb")  # il server apre il file
            outputdata = f.read()  # bufferizza l'intero contenuto del file richiesto in un buffer temporaneo
            # Invia la riga di intestazione HTTP nel socket con il messaggio OK
            connection_socket.send("HTTP/1.1 200 OK\r\n\r\n".encode())
            connection_socket.send(outputdata)
            connection_socket.send("\r\n".encode())
            connection_socket.close()

    except IOError:
        if message.split()[1].decode() == '/':
            # se non viene passato nessun file carico la pagina index.html
            filename = "/index.html"
            f = open(filename[1:], "rb")  # il server apre il file
            outputdata = f.read()  # bufferizza l'intero contenuto del file richiesto in un buffer temporaneo

            # Invia la riga di intestazione HTTP nel socket con il messaggio OK
            connection_socket.send("HTTP/1.1 200 OK\r\n\r\n".encode())
            connection_socket.send(outputdata)
            connection_socket.send("\r\n".encode())
        else:
            # Invia messaggio di risposta per file non trovato
            connection_socket.send(bytes("HTTP/1.1 404 Not Found\r\n\r\n", "UTF-8"))
            connection_socket.send(
                bytes("<html><head></head><body><h1>404 Not Found</h1></body></html>\r\n", "UTF-8"))
        connection_socket.close()


def signal_handler(signal, frame):
    print("\nExiting http server (Ctrl+C pressed)")
    serverSocket.close()
    if connectionSocket:
        connectionSocket.close()
    sys.exit()  # Termina il programma dopo aver inviato i dati corrispondenti


if __name__ == '__main__':
    # assegno alla porta del nostro server il valore 82
    serverPort = 82
    # Legge il numero della porta dalla riga di comando in caso non si volesse utilizzare la 82
    if sys.argv[1:]:
        serverPort = int(sys.argv[1])
    # crea un socket INET di tipo STREAM
    serverSocket = socket(AF_INET, SOCK_STREAM)
    # associa il socket alla porta scelta
    serverSocket.bind(('', serverPort))

    # Do al server la possibilita' di avere 5 connessioni in coda non ancora servite
    serverSocket.listen(5)

    # interrompe l’esecuzione se da tastiera viene inserita la sequenza (CTRL + C)
    signal.signal(signal.SIGINT, signal_handler)

    print("the web server is up on port:", serverPort)
    i = 1
    while True:
        print("\nReady to serve request number " + str(i) + "...\n")
        # Stabilisce la connessione, ossia sul socket si prepara ad accettare connessioni in entrata all'indirizzo e
        # porta definiti
        connectionSocket, addr = serverSocket.accept()
        rc = Thread(target=request_controller, args=(connectionSocket,))
        print("Managing request number " + str(i))
        rc.setDaemon(True)
        rc.start()
        i += 1
