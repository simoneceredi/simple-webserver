# Simple Web Server #


## Startup ##
In order to start the web server just run WebServerCerediSimone.py with python3.
The server will automatically start using the 82 port, it can be changed by giving the choosen one as first and only parameter.

## Connection to the WebServer ##
After starting the WebServer in order to connect from the same PC go to the web browser and connect to 127.0.0.1:82
